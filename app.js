const express = require("express");
const app = express();
const port = 8080;
const session = require("express-session");
const routes = require('./routes/route');

app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: false })); // kirim nya menggunakan form biar bisa kebaca di body
app.use(express.static("public"));

app.use(routes);

app.listen(port, () => {
  console.log(`App listening on port http://localhost:${port}`);
});