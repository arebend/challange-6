const express = require("express");
const router = express.Router();

const {
  user_game,
  user_game_biodata,
  user_game_history
} = require('../models');

router.get('/', (req, res) =>
  res.redirect('dashboard')
)

/** GET CREATE USER **/
router.get('/create-user', (req, res) => {
  res.render('create-user')
})

/** POST CREATE USER **/
router.post('/create-new-users', (req, res) => {
  const {
    username,
    password
  } = req.body;
  // console.log(username,
  //   password);
  if (!username || !password) {
    res.sendStatus(400);
    return
  }

  user_game.create({
      username,
      password,
    })
    .then(result => {
      // res.status(200).json(result);
      // res.status(200).json('succes create new user');
      res.redirect('/dashboard')
    }).catch(err => {
      console.log(err);
      res.sendStatus(500)
    });
});

/** GET DASHBOARD USER **/
router.get('/dashboard', (req, res) => {
  user_game.findAll({})
    .then((result) => {
      // res.status(200).json(result);
      res.render('dashboard', {
        result
        // lempar ke EJS kemudian di loop di EJS
      })
    }).catch((err) => {
      console.log(err);
    });
})

/** GET DETAIL HISTORY USER **/
router.get('/details-history/:id', async (req, res) => {
  const {
    id
  } = req.params;
  const user = await user_game.findOne({
    where: {
      id: id
    },
  })
  // const histories = await user_game_history.findAll({
  //   where: {user_id: id},
  // })
  const biodata = await user_game_biodata.findOne({
    where: {
      user_id: id
    },
  })
  // res.json([user,biodata])
  res.render('details-history', {
    user,
    biodata
  });
});

/** DELETE USER **/
router.get('/delete-users/:id', async (req, res) => {
  const {
    id
  } = req.params;
  console.log('DELETE user_game =>', id);
  await user_game.destroy({
    where: {
      id: id
    }
  })
  // res.json(user_game)
  res.redirect('/dashboard')
})

/** GET EDIT USER **/
router.get('/edit-users/:id', async (req, res) => {
  const {
    id
  } = req.params;
  const user = await user_game.findOne({
    where: {
      id: id
    }
  })
  const biodata = await user_game_biodata.findOne({
    where: {
      user_id: id
    }
  })
  res.render('edit-user', {
    user: user,
    biodata: biodata
  })
})

/** POST EDIT / UPDATE USER **/
router.post('/update/:id', async (req,res) => {
  const {id} = req.params 
  const user = await user_game.findOne({
      where: {
          id:id
      },
      //  include :[{
      //   model : user_game_biodata,
      //   as: "user_game_biodata"
      // }]
  })
  const biodata = await user_game_biodata.findOne({
      where: {
          UserGameId : id
      }
  })
  await user.update(req.body);
  await biodata.update(req.body);
  // res.json(user)
  res.redirect('/dashboard');
})

/** GET BIODATA USER **/
router.get('/add-biodata/:id', async (req, res) => {
  const {
    id
  } = req.params;
  const user = await user_game.findOne({
    where: {
      id: id
    }
  });
  res.render('add-user-biodata', {
    user: user
  });
});

/** POST BIODATA USER **/
router.post('/add-biodata/:id', async (req, res) => {
  const {
    id
  } = req.params;
  const {
    first_name,
    last_name,
    birthday,
    gender,
    location,
    phone,
    user_id
  } = req.body;
  await user_game_biodata.create({
    first_name: first_name,
    last_name: last_name,
    birthday: birthday,
    gender: gender,
    location: location,
    phone: phone,
    user_id: id
  });
  res.redirect("/dashboard");
})


module.exports = router;